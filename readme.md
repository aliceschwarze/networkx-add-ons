# NetworkX Add-Ons

Some extra functions for working with networkx graphs.

To see how these functions work, check out the demo!

## Drawing self-edges

The function `draw_networkx_selfedges()` draws self-edges for a networkX graph. The function uses small hollow circles as self-edges. (Sorry no arrowheads available (yet)!) One can change the position, size, line thickness, line color, etc. via keyword arguments. (See documentation in source code.)

## Optimal permutation layout
For a given layout, the function `nao.optimal_permutation_layout()` finds a permutation of node positions that gives us and _optimal_ layout and returns the permuted layout. With _optimal_ I mean that the sum of edge lengths is as small as possible. You can set _optimal_ to mean something else by making changes to the helper function `nao._layout_cost()`. The function `nao.optimal_layout_permutation()` finds a global optimum by computing the layout cost (i.e., the sum of edge lengths) for every(!) permutation node positions in the layout. Looping over all permutations of nodes scales very badly. On my PC, this works fairly well for graphs up to 6 or 7 nodes. Use for larger networks at your own risk!

I wrote this function with `nx.layout.shell)layout()` in mind, but you can use it for other layouts, too. Layouts  that aim to minimize the total edge length already (e.g., `nx.layout.spring_layout()`) tend to have an optimal ordering of nodes, so running `nao.optimal_permutation_layout()` on these layouts is probably a waste of time.

## Optimal permutation layout with constraints and the 'oriented' shell layout
The function `nao.optimal_permutation_layout_with_constraints()` does exactly the same thing as `nao.optimal_permutation_layout()`, but it allows you fix some node positions. The function `nao.oriented_shell()` uses `nao.optimal_permutation_layout_with_constraints()` to find an optimal shell layout that has a maximum-degree node or a node with a self-edge as its first node (i.e., the node with position (1,0)).

## Sets of nonisomorphic subgraphs
The function `nao.set_of_nonisomorphic_graphs()` returns a list of non-isomorphic graphs for a specified number of nodes and edges. It uses `nx.is_isormophic()` to determine if a pair of graphs is isomorphic. This function is a _flexible_ tool for enumerating motifs in network structure. You can choose whether graphs are directed or undirected and whether they can have self-edges or not.
