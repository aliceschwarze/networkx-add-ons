#    Copyright 2019 by Alice C. Schwarze
#    GNU General Public License (version 3).
#
#
"""
**********
Networkx Add-ons
**********

Some functions for working with networkx graphs.

"""

################################################################################

import networkx as nx
import numpy as np

__all__ = ['hard_copy_networkx_graph',
           'draw_networkx_selfedges',
           '_compute_node_extent',
           'optimal_shell_layout',
           '_layout_cost',
           'optimal_permutation_layout_with_constraints',
           'oriented_shell',
           #'oriented_shell2',
           #'is_isomorphic_via_matrix',
           'nonisomorphic_graphs',
           '_read_pickle',
           '_write_pickle',
           '_filename_for_graphs'
           ]

################################################################################

def hard_copy_networkx_graph(G):
    '''Creates a hard copy of a network graph G. The graph G can be a `nx.Graph` 
    or a `nx.DiGraph` object. 

    Parameters
    ----------
    G : graph
       A networkx graph
       
    Returns
    -------
    nx.Graph or nx.DiGraph H
        a hard-copy of the graph G

    Examples
    --------
    >>> g0 = nx.dodecahedral_graph()
    >>> g0_soft_copy = g0
    >>> # if we know that g0 is nx.Graph or nx.DiGraph
    >>> g0_hard_copy = nx.Graph(g0) #or nx.DiGraph(g0)
    >>> # otherwise
    >>> g0_hard_copy2 = hard_copy_networkx_graph(g0)
    >>> for i, g in enumerate([g0, g0_soft_copy, g0_hard_copy, g0_hard_copy2]):
    >>>    print(i, g.number_of_edges())
    >>> g0.remove_edge(0,1)
    >>> for i, g in enumerate([g0, g0_soft_copy, g0_hard_copy, g0_hard_copy2]):
    >>>    print(i, g.number_of_edges())
    '''
    H = G.__class__()
    H.add_nodes_from(G)
    H.add_edges_from(G.edges)
    return H



def draw_networkx_selfedges(G, pos, node_size=300, shift=1, arc_size=0.5, 
                            orientation='radial', selfedge_color=None, 
                            **kwds):
    '''Draw self-edges for graph G.

    Draws hollow circles behind nodes to visualize self-edges. Parameters 
    set location and size of circle.

    Parameters
    ----------
    G : graph
       A networkx graph

    pos : dictionary
       A dictionary with nodes as keys and positions as values.
       Positions should be sequences of length 2.

    node_size : scalar or array, optional
       Size or sizes of nodes in the G's visualization. The value of  
       node_size should be the same when calling nx.draw(), 
       nw.draw_networkx(), or nx.draw_networkx_nodes() before or after 
       calling draw_networkx_selfedges(). Default is 300.

    shift : scalar, optional
       Sets distance between the center point of a node and the center
       point of its self-edge. Default is 1.

    arc_size : scalar, optional
       Sets ratio between radius of node and radius of self-edge arcs.
       Default is 0.5.

    orientation : [scalar | iterable (dictionary, list, array) | "radial"], optional
       Defines the direction in which self-edge arcs are shifted. Numerical
       values are interpreted as angles in degrees.  A value of 0 degrees
       indicates an orientation parallel to the x-axis. If value is 
       "radial", all self-edge arcs are shifted away from the axis' origin.

    selfedge_color : color string, or array of floats
       Color of self-edges. Default is 'k' or the value of the keyword 
       argument edge_colors if passed.

    linewidths : scalar or sequence, optional
       Line width for self-edge arcs. Default is 1.0.

    Returns
    -------
    matplotlib.collections.PathCollection
        `PathCollection` of the self-edges.

    Examples
    --------
    >>> g = nx.dodecahedral_graph()
    >>> g.add_edges_from([[i,i] for i in range(11)])
    >>> pos = nx.layout.shell_layout(g)
    >>> nx.draw(g,pos)
    >>> draw_networkx_selfedges(g,pos)
    '''

    try:
        import numpy as np
    except ImportError:
        raise ImportError("numpy required for draw_networkx_selfedges()")

    # check if pos is an admissable dictionary of node positions
    keys = pos.keys()
    num_pos = len(keys)
    num_nodes = G.number_of_nodes()
    if num_pos < num_nodes:
        raise Exception('Number of entries in position dictionary needs to be equal to or greater than the number of nodes in G.')

    # radii of nodes in data coordinates
    if isinstance(node_size, (int, float)):
        radius = _compute_node_extent(node_size)
        radii = radius*np.ones((num_nodes,2))
    #elif isinstance(node_size, dict):
    #    try:
    #        radii = np.array([_compute_node_extent(node_size[i]) for i 
    #                          in nodes])
    #    except KeyError:
    #        print('Keys in dictionary node_size do not match nodes in G.')
    elif hasattr(node_size, '__iter__'):
        if len(node_size)==num_nodes:
            radii = np.array([compute_node_extent(node_size[i]) for i 
                              in range(num_nodes)])
        else:
            raise Exception('node_size must be number or array with length equal to number of nodes in G.')
    else:
        raise Exception('node_size must be number or array.')

    # angles of selfedges
    nodes = G.nodes()
    if isinstance(orientation, (int, float)):
        # convert degrees to radiants
        alpha = orientation*np.pi/180.0*np.ones(num_nodes)
    elif isinstance(orientation, dict):
        try:
            alpha = np.pi/180.0*np.array([orientation[i] for i in nodes])
        except KeyError:
            print('Keys in dictionary alpha do not match nodes in G.')
    elif hasattr(orientation, '__iter__'):
        if isinstance(orientation,str):
            if orientation == "radial":
                # compute angles from node positions
                alpha = np.array([np.arctan2(pos[i][1], pos[i][0]) for i 
                                 in pos.keys() if i in nodes]) 
            else:
                raise Exception('Unknown string argument for orientation.')
        elif len(orientation)==num_nodes:
            # convert degrees to radiants
            alpha = np.array(orientation)*np.pi/180.0
        else:
            raise Exception('If orientation is non-string iterable, its length must be equal to number of nodes.')
    else:
        raise Exception('orientation must be number, iterable, or "radial".')

    # compute shifts for each possible selfedge position
    dx = shift*np.cos(alpha)*radii[:,0]
    dy = shift*np.sin(alpha)*radii[:,1]
    pos2 = {k: [pos[k][0]+dx[i], pos[k][1]+dy[i]] for i,k 
            in enumerate(pos.keys()) if k in nodes}
    
    # set color of selfedges
    if selfedge_color is None:
        if 'edge_color' in kwds.keys():
            selfedge_color = kwds['edge_color']
        else:
            selfedge_color = 'k'

    # subgraph with nodes that have selfedges
    edges = G.edges()
    G2 = nx.Graph()
    for n in nodes:
        if (n,n) in edges:
            G2.add_node(n)
            
    # if there are no selfeges return empty list
    if not G2.number_of_nodes():
        return []
    
    # draw selfedges
    edge_collection = nx.draw_networkx_nodes(G2, pos2, 
                                             node_size=node_size*arc_size,
                                             edgecolors=selfedge_color, 
                                             node_color='none', **kwds)
    edge_collection.set_zorder(0)
    return edge_collection



def _compute_node_extent(node_size, figure=None, ax=None):
    '''Compute the radius of a node in units of an axis' x and y axis.
    
    Parameters
    ----------
    node_size : scalar
       The size of a node in the same units as used in nx.draw() or
       nx.draw_networkx_nodes().
       
    fig : figure, optional
       Default is pyplot.gcf().
       
    ax : axis, optional
       Default is pyplot.gca().

    Returns
    -------
    tuple (dx,dy)
        dx is radius in units of x axis, dy is radius in units of y axis.
    '''

    try:
        from matplotlib import pyplot as plt
    except ImportError:
        raise ImportError("Matplotlib required for _compute_node_extent()")

    try:
        import numpy as np
    except ImportError:
        raise ImportError("numpy required for _compute_node_extent()")

    if figure is None:
        figure = plt.gcf()
    if ax is None:
        ax = plt.gca()
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    ax_extent_x = xlims[1]-xlims[0] # extent of x axis
    ax_extent_y = ylims[1]-ylims[0] # extent of y axis 
    ax_width_inch = (ax.get_position()).width*figure.get_size_inches()[0]
    ax_height_inch = (ax.get_position()).height*figure.get_size_inches()[1]

    node_radius_pts = np.sqrt(node_size)/2.0 # node radius in points
    node_radius_inch = node_radius_pts/72.0 # node radius in inch
    node_radius_extent_x = node_radius_inch/ax_width_inch*ax_extent_x
    node_radius_extent_y = node_radius_inch/ax_height_inch*ax_extent_y

    return(node_radius_extent_x, node_radius_extent_y)


def optimal_permutation_layout(G, positions):
    '''Find optimal permutaion of nodes for a given layout.
    
    For a set of given node positions, find permutation of nodes that 
    minimizes the total length of edges in a graph's visualization.
    
    Using this for networks with more than 10ish nodes is a bad idea!
    
    Parameters
    ----------
    G : graph
       A networkx graph.
       
    positions : dictionary
       A dictionary with nodes as keys and positions as values.
       Positions should be sequences of length 2.
       
    Returns
    -------
    tuple (new_pos, c)
        new_pos is dictionary of node positions, c is the associated
        total length of edges in visualization.
        
    Examples
    --------
    >>> g = nx.Graph()
    >>> g.add_edges_from([[0,3],[1,3],[1,4],[2,3],[3,5]])
    >>> pos = nx.layout.shell_layout(g)
    >>> pos2, c = optimal_permutation_layout(g, pos)
    >>> nx.draw(g, pos2)
    '''
    
    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for optimal_permutation_layout_with_constraints()")
    
    edges = G.edges()
    node_labels = positions.keys()
    lowest_cost = 1E10
    for p in itertools.permutations(node_labels):
        current_cost = _layout_cost(G, {i:positions[p[i]] for i in node_labels})
        if lowest_cost > current_cost:
            lowest_cost = current_cost
            lowest_cost_p = p
    # new positions
    pos_new = {i:positions[lowest_cost_p[i]] for i in node_labels}
    return pos_new, lowest_cost



def _layout_cost(G, positions):
    '''Computes total length of edges in a graph's visualization.
    
    Parameters
    ----------
    G : graph
       A networkx graph.
       
    positions : dictionary
       A dictionary with nodes as keys and positions as values.
       Positions should be sequences of length 2.
       
    Returns
    -------
    float c
       Total edge length in a visualization of a graph.
    '''

    try:
        import numpy as np
        from numpy.linalg import norm as vector_norm
    except ImportError:
        raise ImportError("numpy required for _layout_cost()")

    n = G.number_of_nodes()
    edges = G.edges()
    out = np.sum([vector_norm(positions[i]-positions[j]) for i in range(n) 
                  for j in range(i) if ((i,j) in edges or (j,i) in edges)])
    return out



def optimal_permutation_layout_with_constraints(G, positions, fixed_nodes):
    '''Find optimal permutaion of nodes for a given layout with 
    constraints.
    
    For a set of given node positions, find allowed permutation of nodes
    that minimizes the total length of edges in a graph's visualization. 
    One can force a subset of nodes to keep their position.
    
    Using this for networks with more than 10ish nodes is a bad idea!
    
    Parameters
    ----------
    G : graph
       A networkx graph.
       
    positions : dictionary
       A dictionary with nodes as keys and positions as values.
       Positions should be sequences of length 2.
       
    fixed_nodes : list or array
       List of node labels of nodes that should keep their position.
       If fixed_nodes is [], this function outputs the same as 
       optimal_permutation_layout() but can take longer to compute.
       
    Returns
    -------
    dict new_pos
        dictionary of node positions
        
    float c
        c is the associated total length of edges in the visualization
        
    Examples
    --------
    >>> g = nx.Graph()
    >>> g.add_edges_from([[0,3],[1,3],[1,4],[2,3],[3,5]])
    >>> pos = nx.layout.shell_layout(g)
    >>> pos2, c = optimal_permutation_layout(g, pos, [1,2])
    >>> nx.draw(g, pos2)
    '''
    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for optimal_permutation_layout_with_constraints()")
        
    edges = G.edges()
    node_labels = positions.keys()
    flexible_nodes = [x for x in node_labels if x not in fixed_nodes]
    permuted_positions = dict(positions)
    lowest_cost = 1E10
    for p in itertools.permutations(flexible_nodes):
        for i, n in enumerate(flexible_nodes):
            permuted_positions[n] = positions[p[i]]
        current_cost = _layout_cost(G, permuted_positions)
        if lowest_cost > current_cost:
            lowest_cost = current_cost
            lowest_cost_p = p
    # new positions
    for i, n in enumerate(flexible_nodes):
        permuted_positions[n] = positions[lowest_cost_p[i]]
    return permuted_positions, lowest_cost


def oriented_shell(G, select="degree"): 
    '''Find optimal shell layout with a select node on the right.
    
    Find the shell layout for a graph G that minimizes the total length
    of edges when a node with maximum degree or a node with a selfedge is
    in the far right position.
    
    Using this for networks with more than 10ish nodes is a bad idea!
    
    Parameters
    ----------
    G : graph
       A networkx graph.
       
    select : ["degree" | "self-edge"], optional
       When select is "degree" (or "d"), the node on the far right is a 
       node with maximum degree. When select is "self-edge" (or "se"), 
       the node on the far right is a node with a self-edge. Default is
       "degree".
              
    Returns
    -------
    dict new_pos
        new_pos is dictionary of node positions
        
    float lowest_cost
        Total edge length associated with the final layout
        
    Examples
    --------
    >>> g = nx.Graph()
    >>> g.add_edges_from([[0,3],[1,3],[1,4],[2,3],[3,5],[1,1],[2,2]])
    >>> pos = oriented_shell(g)
    >>> nx.draw(g, pos)
    '''

    try:
        import numpy as np
        from numpy.linalg import norm as vector_norm
    except ImportError:
        raise ImportError("numpy required for oriented_shell()")

    # position for node with selfedge
    position0 = np.array([1.0,0.0])

    # generate positions for nodes in shell layout
    shell = nx.layout.shell_layout(G)

    # set criterion for special nodes
    edges = G.edges()
    if select in ["degree", "deg", "d"]:
        max_deg = np.max(np.array(G.degree())[:,1])
        special_nodes =  [n for n in G.nodes() if G.degree(n)==max_deg]
    elif select in ["self-edge", "self_edge", "selfedge", "se"]:
        # mark nodes with selfedges as special nodes
        special_nodes = [n for n in G.nodes() if (n,n) in edges]
    else:
        raise Exception('Unknown value for keyword argument select. Choose "degree" (or "d") or "self-edge" (or "se").')
    if len(special_nodes)==0:
        # there is nothing to orient here
        # return an optimal shell layout without constraints
        return optimal_permutation_layout(G, shell)[0]

    # find the node that currently has the position that a sepcial node 
    # should have
    for k in shell.keys():
        # allow for small numeric error
        if vector_norm(shell[k]-position0) < 1E-6:
            node0, position0 = k, shell[k]
            break 

    # try different node switches
    lowest_cost = 1E10
    for node1 in special_nodes:
        #hard copy position dictionary
        shell2 = dict(shell)
        # switch node positions so that node1 is on the right        
        shell2[node0] = shell[node1]
        shell2[node1] = position0
        # get optimal shell with node1 on the right
        os, c = optimal_permutation_layout_with_constraints(G, shell2, 
                                                            [node1])
        if lowest_cost > c:
            lowest_cost = c
            lowest_cost_shell = os

    return lowest_cost_shell, lowest_cost



def set_of_nonisomorphic_graphs(n, m, directed=True, selfedges=True,
                                isolates=False, save=True, load=True, 
                                filename=None, directory=''): 
    '''Find the set of all non-isomorphic graphs with n nodes and m edges.
    
    Perform an exhaustive search for for graphs that are non-isomorphic to each 
    other. Returns the largest possible list of non-isomorphic graphs (nx.Graph 
    objects of nx.DiGraph objects) with specified numbers of nodes and edges.
    
    Parameters
    ----------
    n : int
       Number of nodes

    m : int
       Number of edges
    
    directed : bool
       If true, consider graphs to be directed and return a list of nx.DiGraph
       objects
    
    selfedges : bool
       If true, allow graphs to have self-edges
       
    save : bool
       If true, save list of nonisomorphic graphs to file
    
    load : bool
       If true, try loading list of nonisomorphic graphs from file instead of 
       computing the list from scratch
       
    filename : str
       Filename to be used for save and load options
    
    Returns 
    -------
    list graphs
       A list of non-isomorphic graphs with n nodes and m edges
       
    Example
    -------
    >>> list_of_graphs = set_of_nonisomorphic_graphs(3,3, directed=False) 
    >>> plt.figure(figsize=(6,4))
    >>> for i,g in enumerate(list_of_graphs):
    >>>     ax = plt.subplot(2,3,i+1)
    >>>     ax.set_xlim([-1.25,1.75])
    >>>     ax.set_ylim([-1.5,1.5])
    >>>     shell = nx.layout.shell_layout(g)
    >>>     nx.draw(g, shell)
    >>>     draw_networkx_selfedges(g, shell)
    >>>     ax.axis('on')
    '''
    try:
        import os
    except ImportError:
        raise ImportError("os required for set_of_nonisomorphic_graphs()")

    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for set_of_nonisomorphic_graphs()")
        
    try:
        import numpy as np
    except ImportError:
        raise ImportError("numpy required for set_of_nonisomorphic_graphs()")

    if filename is None:
        #generate file name
        filename = _filename_for_graphs('nonisomorphic_graphs', n, m, directed, 
                                        selfedges) + '.p'
    
    if load and os.path.exists(directory+filename):
        # return data that has been computed previously
        graphs = _read_pickle(directory+filename)
        return graphs
    
    # set graph type and free slots for edges
    if directed:
        cu = nx.DiGraph()
        if selfedges:
            free_slots = n**2
            row_indices = np.array([i//n for i in range(free_slots)])
            col_indices = np.array([i % n for i in range(free_slots)])
            slots_list = zip(row_indices, col_indices)
        else:

            free_slots = n*(n-1)
            row_indices =  np.array([i//(n-1) for i in range(free_slots)])
            col_indices =  np.array([i % (n-1) + 1*(i % (n-1) >= i//(n-1)) 
                                     for i in range(free_slots)])
            slots_list = zip(row_indices, col_indices)
    else:
        cu = nx.Graph()
        if selfedges:
            #free_slots = n*(n+1)//2
            row_indices, col_indices = np.triu_indices(n,0)
            slots_list = zip(row_indices, col_indices)
        else:
            #free_slots = n*(n-1)//2
            row_indices, col_indices = np.triu_indices(n,1)
            slots_list = zip(row_indices, col_indices)

    # create first graph of the specified type with n nodes and m edges
    #g = nx.empty_graph(n, create_using=cu)    
    #g.add_edges_from(list(zip(row_indices, col_indices))[:m])

    # store first graph in a list of non-isomorphic graphs
    graphs = [] #hard_copy_networkx_graph(g)]
 
    # go through all possible combinations of m edges in specified graph type
    for c in itertools.combinations(slots_list, m):
        
        # create new graph with this combination of edges
        g = nx.empty_graph(n, create_using=cu)    
        g.add_edges_from(c)
        
        if not isolates:
            # remove graphs with isolated nodes
            if min([g.degree(x) for x in g.nodes()])==0:
                continue
        
        #check if new graph  g is isomporphic to all previously stored ones
        isotest = lambda x: nx.is_isomorphic(g,x) 
        if not np.any(list(map(isotest, graphs))): 
            # if so, add a hard copy of g to list of non-isomorphic graphs
            graphs = graphs + [hard_copy_networkx_graph(g)]

    if save:
        # if save==True, save data for later
        _write_pickle(directory+filename, graphs) 
            
    return graphs



def _write_pickle(fname, obj):
    '''Write or overwrite a pickle file. Yes I am lazy.
    
    Parameters
    ----------
    fname : str
       location and name of pickle file to be created or overwritten
       
    obj : any object that can be stored in a pickle
    
    Returns 
    -------
    str fname
       The location and name of the created pickle
    '''

    try:
        import pickle
    except ImportError:
        raise ImportError("pickle required for _write_pickle()")

    outfile = open(fname,'wb')
    pickle.dump(obj,outfile)
    outfile.close()
    return fname



def _read_pickle(fname):
    '''Read a pickle file. Yes I am lazy.
    
    Parameters
    ----------
    fname : str
       location and name of pickle file to be read
       
    Returns 
    -------
    object obj
       Whatever we found in the pickle
    '''
    
    try:
        import pickle
    except ImportError:
        raise ImportError("pickle required for _read_pickle()")

    outfile = open(fname,'rb')
    obj = pickle.load(outfile)
    outfile.close()
    return obj



def _filename_for_graphs(s, num_nodes, num_edges, directed, selfedges, 
                         extension=''):
    '''Returns a filename for a given set of graph attributes. This function 
    exists because I am lazy AF.
    
    Parameters
    ----------
    s : str
       Beginning of the file name.
    
    num_nodes : int
       Number of nodes in graph(s).

    num_edges : int
       Number of edges in graph(s).

    directed : bool
        If true, file name includes the string 'directed'

    selfedges : bool
        If true, file name includes the string 'selfedges'
    
    extension : string (optional)
        End of file name. (Should include a '.')
              
    Returns
    -------
    str fname
        a file name
    '''
    
    fname = s + str(num_nodes)+'_'+str(num_edges)
    if directed: fname += '_directed'
    if selfedges: fname += '_selfedges'
    fname = fname + extension
    
    return fname



'''
def oriented_shell2(G, orientation='east'): 
    # Force all loops to be as much to the right as possible. 
    # This function does not work and is probably useless.
    
    try:
        from numpy.linalg import norm as vector_norm
    except ImportError:
        raise ImportError("numpy required for oriented_shell()")

    # position for node with selfedge
    if orientation=='west':
        position0 = np.array([-1.0,0.0])
    elif orientation=='east':
        position0 = np.array([1.0,0.0])
    else:
        raise Exception('Keyword argument orientation must be "east" or "west".')

    # generate positions for nodes in shell layout
    shell = nx.layout.shell_layout(G)

    # find nodes with selfedges:
    edges = G.edges()
    nodes_with_selfedge = [n for n in G.nodes() if (n,n) in edges]
    num_selfedges = len(nodes_with_selfedge)

    if num_selfedges==0:
        # there is nothing to orient here
        # return an optimal shell layout without constraints
        return optimal_permutation_layout(G, shell)[0]

    # find the nodes that currently have the position that the nodes with selfedge should have
    if orientation=='west':
        nodes_to_be_shifted = sorted(shell, key=shell.get)[:num_selfedges]
    elif orientation=='east':
        #np.sort(np.array(list(shell.values)), axis=0)[-num_selfedges:]
        nodes_to_be_shifted = sorted(shell, key=lambda x: shell[x][0])[::-1][:num_selfedges]

    # try switches
    lowest_cost = 1E10
    for p in itertools.permutations(nodes_to_be_shifted):
        #hard copy positions dictionary
        permuted_shell = dict(shell)
        # switcharoo
        for i in range(num_selfedges):
            permuted_shell[nodes_with_selfedge[i]] = shell[p[i]]
            permuted_shell[p[i]] = shell[nodes_with_selfedge[i]]
        new_shell, cost = optimal_permutation_layout_with_constraints(G, permuted_shell, nodes_with_selfedge)
        if lowest_cost > cost:
            lowest_cost = cost
            lowest_cost_shell = new_shell

    return lowest_cost_shell, lowest_cost
'''



    
    
